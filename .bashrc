#!/usr/bin/env bash

# Author: Vadim Evgenyevich
# GitLab: https://www.gitlab.com/ve.toff

# | EXPORTS
# |--> use proper colors
export TERM="xterm-256color"
# |--> ignore/erase duplicates in bash history
export HISTCONTROL=ignoredups:erasedups 
# |--> $EDITOR use neovim in terminal mode
export EDITOR="nvim" 

export XDG_CONFIG_HOME="$HOME/.config"
export XDG_CACHE_HOME="$HOME/.cache"
export XDG_DATA_HOME="$HOME/.local/share"
export XDG_STATE_HOME="$HOME/.local/state"

export ZDOTDIR="$XDG_CONFIG_HOME/zsh"

[[ $- != *i* ]] && return

if [ -x /usr/bin/dircolors ]; then
    [ -r ~/.dircolors ] && eval "$(dircolors -b ~/.dircolors)" || eval "$(dircolors -b)"
fi

# | PROMPT
function short_pwd {
	pwd | sed s.$HOME.~.g | awk -F"/" '
	  BEGIN { ORS="/" }
	  END {
	  for (i=1; i<= NF; i++) {
	      if ((i == 1 && $1 != "") || i == NF-1 || i == NF) {
	        print $i
	      }
	      else if (i == 1 && $1 == "") {
	        print "/"$2
	        i++
	      }
	      else {
	        print ".."
	      }
	    }
	  }'
}

function we_are_in_git_work_tree {
    git rev-parse --is-inside-work-tree &> /dev/null
}

function parse_git_branch {
    if we_are_in_git_work_tree
    then
    local BR=$(git rev-parse --symbolic-full-name --abbrev-ref HEAD 2> /dev/null)
   	if [ -z "$BR" ]; then 
			echo -n ''
		else
			if [ "$BR" == HEAD ]; then
    	    local NM=$(git name-rev --name-only HEAD 2> /dev/null)
    	    if [ "$NM" != undefined ]
    	    then echo -n "@$NM"
    	    else git rev-parse --short HEAD 2> /dev/null
    	    fi
    	else
				echo -n "($BR)"
    	   fi
    	fi
		fi
}

if [ "$EUID" -eq 0 ]; then
# |--> root
	PS1='\[\e[0;3;91m\]\u\[\e[0;3m\]@\[\e[0;2;3m\]\h \[\e[0m\]\[\e[0;38;5;202m\]$(short_pwd)\[\e[0m\] \[\e[0;92m\]$(parse_git_branch) \[\e[0m\]\$ \[\e[0m\]'
else
# |--> user
	PS1='\[\e[0;3m\]\u\[\e[0;3m\]@\[\e[0;2;3m\]\h \[\e[0m\]\[\e[0;38;5;202m\]$(short_pwd)\[\e[0m\] \[\e[0;92m\]$(parse_git_branch) \[\e[0m\]\$ \[\e[0m\]'
fi

# | CHANGE THE WINDOW TITLE FOR TERMINALS
case ${TERM} in
	xterm*|rxvt*|Eterm*|aterm|kterm|gnome*|interix|konsole*)
		PROMPT_COMMAND='echo -ne "\033]0;${USER}@${HOSTNAME%%.*}:${PWD/#$HOME/\~}\007"'
		;;
	screen*)
		PROMPT_COMMAND='echo -ne "\033_${USER}@${HOSTNAME%%.*}:${PWD/#$HOME/\~}\033\\"'
		;;
esac

# | SHOPT
# |--> change to named directory
shopt -s autocd
# |--> autocorrects cd misspellings
shopt -s cdspell 
# |--> save multi-line commands in history as single line
shopt -s cmdhist 
shopt -s dotglob
# |--> do not overwrite history
shopt -s histappend 
# |--> expand aliases
shopt -s expand_aliases 
# |--> checks term size when bash regains control
shopt -s checkwinsize 

# | ARCHIVE EXTRACTION (usage: ex <archive>)
ex ()
{
  if [ -f $1 ] ; then
    case $1 in
      *.tar.bz2)   tar xjf $1   	;;
      *.tar.gz)    tar xzf $1   	;;
      *.bz2)       bunzip2 $1   	;;
      *.rar)       unrar x $1   	;;
      *.gz)        gunzip $1    	;;
      *.tar)       tar xf $1    	;;
      *.tbz2)      tar xjf $1   	;;
      *.tgz)       tar xzf $1   	;;
      *.zip)       unzip $1     	;;
      *.Z)         uncompress $1	;;
      *.7z)        7z x $1      	;;
      *)           echo "'$1' cannot be extracted via ex()" ;;
    esac
  else
    echo "'$1' is not a valid file"
  fi
}

[ -r /usr/share/bash-completion/bash_completion ] && . /usr/share/bash-completion/bash_completion
# |- IGNORE UPPER/LOWER CASE WHEN TAB COMPLETION
bind "set completion-ignore-case on"

# IMPORT ALIASES
source $XDG_CONFIG_HOME/aliases

